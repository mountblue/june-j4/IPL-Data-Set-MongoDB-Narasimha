let expect = require("chai").expect;
let path = require("path");
let file = path.resolve("solutions.js");
let data = require(file);

describe("Most Man of The Match Awards throughout all the years of IPL.", function(){
    it("Test Case 1: Positive Test Case", async function(){
        const dataBaseName = "IPLtest";
        const collectionName = "Question5";
        const expectedResult = [ 
            {_id: 'Yuvraj Singh', count: 4},
            {_id: 'GJ Maxwell', count: 3},
            {_id: 'Sachin Tendulkar', count: 2},
            {_id: 'SV Samson', count: 1}];

      

        const actualResult = await data.getMostMOTM(dataBaseName, collectionName);
        expect(actualResult).deep.equals(expectedResult);
    })

    it("Test Case 2: Wrong values 1", async function(){
        const dataBaseName = "IPLtest";
        const collectionName = "Question5";
        const expectedResult = [ 
            {_id: 'Yuvraj Singh', count: 2},
            {_id: 'GJ Maxwell', count: 4},
            {_id: 'Sachin Tendulkar', count: 1},
            {_id: 'SV Samson', count: 2}];

        const actualResult = await data.getMostMOTM(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 3: Empty values", async function(){
        const dataBaseName = "IPLtest";
        const collectionName = "Question5";
        const expectedResult = [];

        const actualResult = await data.getMostMOTM(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 4: Wrong DB", async function(){
        const dataBaseName = "IPLtest2481";
        const collectionName = "Question5";
        const expectedResult = [ 
            {_id: 'Yuvraj Singh', count: 4},
            {_id: 'GJ Maxwell', count: 3},
            {_id: 'Sachin Tendulkar', count: 2},
            {_id: 'SV Samson', count: 1}];

        const actualResult = await data.getMostMOTM(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })
})