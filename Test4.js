let expect = require("chai").expect;
let path = require("path");
let file = path.resolve("solutions.js");
let data = require(file);
const dataBaseName = "IPLtest";
const collectionMatches = "Question4m";
const collectionDeliveries = "Question4d";
const seasonToCheck = 2015;


describe("Extra runs conceded by the teams in the 2016 season of IPL", function () {
    it("Test Case 1: Positive Test Case", async function () {
        
        const expectedResult = [ 
            { Bowler: 'DL Chahar', Balls: 6, Runs: 6, Economy: 6 },
            { Bowler: 'AB Dinda', Balls: 7, Runs: 10, Economy: 8.571428571428571 } ];
      

        const actualResult = await data.getTopEconomicalBowlers(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).deep.equals(expectedResult);
    })

    it("Test Case 2: Wrong values 1", async function () {
        const expectedResult = [ 
            {Bowler: 'DL Chahar', Balls: 7, Runs: 10, Economy: -30 },
            {Bowler: 'AB Dinda', Balls: 6,Runs: 1, Economy: 0.8571428571428571 }];

        const actualResult = await data.getTopEconomicalBowlers(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 3: Empty values", async function () {
        const expectedResult = [];

        const actualResult = await data.getTopEconomicalBowlers(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).not.equals(expectedResult);
    })

})