const express = require("express");
const path = require("path");
const databaseName = "IPL";
const collectionMatches = "matches";
const collectionDeliveries = "deliveries";
const fileName = path.resolve("solutions.js");
const operations = require(fileName);

const app = express();
app.use("/assets", express.static("assets"));
app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
})

app.get("/getNumberOfMatchesPerYear", async (req, res) => {
    const data = await operations.getNumberOfMatchesPerYear(databaseName, collectionMatches);
    res.send(data);
});

app.get("/getMatchesPlayedPerTeam", async (req, res) => {
    const data = await operations.getMatchesPlayedPerTeam(databaseName, collectionMatches);
    res.send(data);
});

app.get("/getExtraRunsConceded", async (req, res) => {
    const data = await operations.getExtraRunsConceded(databaseName, collectionMatches, collectionDeliveries, 2016);
    res.send(data);
});

app.get("/getTopEconomicalBowlers", async (req, res) => {
    const data = await operations.getTopEconomicalBowlers(databaseName, collectionMatches, collectionDeliveries, 2015);
    res.send(data);
});

app.get("/getMostMOTM", async (req, res) => {
    const data = await operations.getMostMOTM(databaseName, collectionMatches);
    res.send(data);
});

app.listen(8080);
console.log("Server running...");