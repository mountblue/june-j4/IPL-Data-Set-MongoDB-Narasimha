var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017";


// 1. Plot the number of matches played per year of all the years in IPL.
function getNumberOfMatchesPerYear(dataBaseName, collectionMatches) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                console.log("Connection established...");
                const database = data.db(dataBaseName);

                cursor = database.collection(collectionMatches).aggregate([{
                            $group: {
                                "_id": "$season",
                                count: {
                                    $sum: 1
                                }
                            }
                        },
                        {
                            $sort: {
                                "_id": 1
                            }
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}

// 2. Matches won per team per season throughout all the years of IPL.
function getMatchesPlayedPerTeam(dataBaseName, collectionMatches) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                console.log("Connection established...");
                const database = data.db(dataBaseName);

                cursor = database.collection(collectionMatches).aggregate([{
                            $group: {
                                _id: {
                                    year: "$season",
                                    team: "$winner"
                                },
                                count: {
                                    $sum: 1
                                }
                            }
                        },
                        {
                            $project: {
                                season: "$_id.year",
                                team: "$_id.team",
                                count: "$count"

                            }
                        },
                        {$group:{
                            _id: "$team",
                            teamDetails: {
                                $push: {
                                    noOfMatches: "$count",
                                    year: "$season"

                                }
                            }
                        }
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}



// 3. For the year 2016 plot the extra runs conceded per team.
function getExtraRunsConceded(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck) {
    return new Promise(function (resolve, reject) {

        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {

                console.log("Connection established...");
                const database = data.db(dataBaseName);
                cursor = database.collection(collectionMatches).aggregate([{
                            $lookup: {
                                from: collectionDeliveries,
                                localField: "id",
                                foreignField: "match_id",
                                as: "joined"
                            }
                        },
                        {
                            $match: {
                                season: seasonToCheck
                            }
                        },
                        {
                            $unwind: "$joined"
                        },
                        {
                            $project: {
                                _id: 0,
                                season: 1,
                                "joined.bowling_team": 1,
                                "joined.extra_runs": 1
                            }
                        },
                        {
                            $group: {
                                "_id": "$joined.bowling_team",
                                "extraRunsConceded": {
                                    $sum: "$joined.extra_runs"
                                }
                            }
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}


// 4. For the year 2015 plot the top economical bowlers.
function getTopEconomicalBowlers(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                console.log("Connection established...");
                const database = data.db(dataBaseName);
                cursor = database.collection(collectionMatches).aggregate([{
                            $match: {
                                season: seasonToCheck
                            }
                        },
                        {
                            $lookup: {
                                from: collectionDeliveries,
                                localField: 'id',
                                foreignField: 'match_id',
                                as: 'deliveries'
                            }
                        },
                        {
                            $unwind: '$deliveries'
                        },
                        {
                            $group: {
                                _id: '$deliveries.bowler',
                                Runs: {
                                    $sum: '$deliveries.total_runs'
                                },
                                Balls: {
                                    $sum: 1
                                }
                            }
                        },
                        {
                            $project: {
                                _id: 0,
                                Bowler: '$_id',
                                Balls: '$Balls',
                                Runs: '$Runs',
                                Economy: {
                                    $multiply: [{
                                        $divide: ['$Runs', '$Balls']
                                    }, 6]
                                }
                            }
                        },
                        {
                            $sort: {
                                Economy: 1
                            }
                        },
                        {
                            $limit: 10
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}






// 5. 10 players who got the most Man of The Match awards.
function getMostMOTM(dataBaseName, collectionName) {
    return new Promise(function (resolve, reject) {
        var cursor = [];

        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, data) {
            if (err) {
                reject(err);
            } else {
                console.log("Connection established...");
                const database = data.db(dataBaseName);

                cursor = database.collection(collectionName).aggregate([{
                            $group: {
                                "_id": "$player_of_match",
                                count: {
                                    $sum: 1
                                }
                            }
                        },
                        {
                            $sort: {
                                "count": -1
                            }
                        },
                        {
                            $limit: 10
                        }
                    ])
                    .toArray(
                        (function (err, docs) {
                            data.close();
                            // console.log(docs);
                            resolve(docs);
                        }));
            }
        });
    });
}

// getNumberOfMatchesPerYear("IPL", "matches");  
// getMostMOTM("IPL","matches");
// getExtraRunsConceded("IPL", "matches", "deliveries",2016);
// getMatchID("IPL","matches",2016);
// getMatchesPlayedPerTeam("IPLtest", "Question2");
// getTopEconomicalBowlers("IPL", "matches", "deliveries", 2015);

module.exports = {
    getNumberOfMatchesPerYear: getNumberOfMatchesPerYear,
    getMostMOTM: getMostMOTM,
    getExtraRunsConceded: getExtraRunsConceded,
    getTopEconomicalBowlers: getTopEconomicalBowlers,
    getMatchesPlayedPerTeam: getMatchesPlayedPerTeam
}