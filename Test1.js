let expect = require("chai").expect;
let path = require("path");
let file = path.resolve("solutions.js");
let data = require(file);

describe("Number of matches played per year of all the years in IPL.", function(){
    it("Test Case 1: Positive Test Case", async function(){
        const dataBaseName = "IPLtest";
        const collectionName = "Question1";
        const expectedResult = [
            {_id: 2015, count: 1},
            {_id: 2016, count: 2},
            {_id: 2017, count: 2},];

        const actualResult = await data.getNumberOfMatchesPerYear(dataBaseName, collectionName);
        expect(actualResult).deep.equals(expectedResult);
    })

    it("Test Case 2: Wrong values", async function(){
        const dataBaseName = "IPLtest";
        const collectionName = "Question1";
        const expectedResult = [
            {_id: 20120, count: 0},
            {_id: 2016, count: 1},
            {_id: 2014, count: 3},];

        const actualResult = await data.getMatchesPlayedPerTeam(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 3: Wrong values", async function(){
        const dataBaseName = "IPLtsadfafest";
        const collectionName = "Question1";
        const expectedResult = [
            {_id: 2015, count: 1},
            {_id: 2016, count: 0},
            {_id: 2017, count: 2},];

        const actualResult = await data.getMatchesPlayedPerTeam(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 4: Expected empty", async function(){
        const dataBaseName = "IPLtest2481";
        const collectionName = "Question1"; 
        const expectedResult = [];

        const actualResult = await data.getMatchesPlayedPerTeam(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })

    xit("Test Case 5: Expected empty", async function(){
        const dataBaseName = "IPLtest";
        const collectionName = "Question1"; 
        const expectedResult = "MongoNetworkError: failed to connect to server [localhost:27017] on first connect [MongoNetworkError: connect ECONNREFUSED 127.0.0.1:27017";

        const actualResult = await data.getMatchesPlayedPerTeam(dataBaseName, collectionName);
        expect(actualResult).equals.equals(expectedResult);
    })
})