let expect = require("chai").expect;
let path = require("path");
let file = path.resolve("solutions.js");
let data = require(file);
const dataBaseName = "IPLtest";
const collectionName = "Question2";


describe("Number of matches played per year of all the years in IPL.", function () {
    it("Test Case 1: Positive Test Case", async function () {
        const expectedResult = [{
                _id: 'Royal Challengers Bangalore',
                teamDetails: [{
                    noOfMatches: 1,
                    year: 2015
                }]
            },

            {
                _id: 'Sunrisers Hyderabad',
                teamDetails: [{
                        noOfMatches: 1,
                        year: 2017
                    },
                    {
                        noOfMatches: 2,
                        year: 2016
                    }
                ]
            },

            {
                _id: 'Kings XI Punjab',
                teamDetails: [{
                        noOfMatches: 2,
                        year: 2016
                    },
                    
                    {
                        noOfMatches: 1,
                        year: 2017
                    },
                    {
                        noOfMatches: 2,
                        year: 2015
                    }
                ]
            }
        ];


        const actualResult = await data.getMatchesPlayedPerTeam(dataBaseName, collectionName);
        // console.log(actualResult);
        expect(actualResult).to.deep.equals(expectedResult);
    })

    it("Test Case 2: Wrong values", async function () {
        const dataBaseName = "IPLtest";
        const collectionName = "Question1";
        const expectedResult = [{
                _id: {
                    year: 2016,
                    team: 'Kings XI Punjab'
                },
                count: 4
            },
            {
                _id: {
                    year: 2017,
                    team: 'Sunrisers Hyderabad'
                },
                count: 9
            },
            {
                _id: {
                    year: 2017,
                    team: 'Kings XI Punjab'
                },
                count: 10
            },
            {
                _id: {
                    year: 2016,
                    team: 'Sunrisers Hyderabad'
                },
                count: 4
            },
            {
                _id: {
                    year: 2015,
                    team: 'Kings XI Punjab'
                },
                count: 10
            },
            {
                _id: {
                    year: 2015,
                    team: 'Royal Challengers Bangalore'
                },
                count: 11
            }
        ];

        const actualResult = await data.getNumberOfMatchesPerYear(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 3: Wrong values", async function () {
        const dataBaseName = "IPLtsadfafest";
        const collectionName = "Question1";
        const expectedResult = [];

        const actualResult = await data.getNumberOfMatchesPerYear(dataBaseName, collectionName);
        expect(actualResult).not.equals(expectedResult);
    })
})