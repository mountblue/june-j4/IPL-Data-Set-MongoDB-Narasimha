$(document).ready(function () {

    //First question.
    fetch("getNumberOfMatchesPerYear")
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            console.log(myJson.map(match => match._id))
            var chart = {
                type: "column"
            };

            var title = {
                text: "Number of matches played per year of all the years in IPL."
            };

            var xAxis = {

                catrgories: myJson.map(a => a._id),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: "Matches played"
                }
            };

            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };

            var series = [{
                name: ["Seasons"],
                data: myJson.map(match => match.count)
            }];

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $("#chart1").highcharts(json);
        });


    //Second Question
    fetch("getMatchesPlayedPerTeam")
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            
            let teamWonData = {},
                years = [],
                seriesData = [];
            for (let i = 0; i < myJson.length; i++) {
                if (myJson[i].hasOwnProperty("_id")) {
                    let teamName = myJson[i]["_id"];
                    if (teamName.length > 0){
                        teamWonData[teamName] = {};
                        for (let yr = 2008; yr <= 2017; yr++) {
                            years.push(yr)
                            teamWonData[teamName][yr] = 0;
                        }
                        myJson[i]["teamDetails"].forEach(function (yearData) {
                            teamWonData[teamName][yearData.year] = yearData.noOfMatches;
                        })
                    }
                }
            }

            for (team in teamWonData) { 
                seriesData.push({
                    name: team,
                    data: Object.values(teamWonData[team])
                })
            }


            // console.log(teamName);
            var chart = {
                type: "bar"
            };

            var title = {
                text: "Matches won by each team in each season"
            };

            var xAxis = {
                categories: years,
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: "Matches played"
                }
            };

            var plotOptions = {
                series: {
                    stacking: "normal"
                }
            };

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = seriesData;
            json.plotOptions = plotOptions;
            $("#chart2").highcharts(json);
        });


    //     // Third Question
    fetch("getExtraRunsConceded")
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            var chart = {
                type: "column"
            };

            var title = {
                text: "Extra Runs Conceded Per Team in the year 2016"
            };

            var xAxis = {
                categories: myJson.map(match => match._id),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: "Extra Runs Conceded"
                }
            };

            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };

            var series = [{
                name: ["Teams"],
                data: myJson.map(match => match.extraRunsConceded),
            }];


            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $("#chart3").highcharts(json);

        });


    //     //Fourth Question
    fetch('/getTopEconomicalBowlers')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {
            var chart = {
                type: 'column'
            };
            var title = {
                text: 'Top Economical Bowlers'
            };
            // var subtitle = {
            //     text: 'Source: Ipl  (Kaggle)'
            // };
            var xAxis = {
                categories: myJson.map(a => a.Bowler),
                crosshair: true
            };
            var yAxis = {
                min: 0,
                title: {
                    text: 'Economy Rate'
                }
            };
            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };
            var series = [{
                name: ['Players'],
                data: myJson.map(a => a.Economy)
            }];

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $('#chart4').highcharts(json);
        });


    //     //Fifth question.
    fetch('/getMostMOTM')
        .then(function (response) {
            return response.json();
        })
        .then(function (myJson) {

            var chart = {
                type: 'column'
            };
            var title = {
                text: "Most Man Of The Match Awards"
            };

            var xAxis = {
                categories: myJson.map(a => a._id),
                crosshair: true
            };

            var yAxis = {
                min: 0,
                title: {
                    text: 'Man Of The Match Awards'
                }
            };

            var plotOptions = {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            };

            var series = [{
                name: ['Players'],
                data: myJson.map(a => a.count)
            }];

            var json = {};
            json.chart = chart;
            json.title = title;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.series = series;
            json.plotOptions = plotOptions;
            $('#chart5').highcharts(json);
        });
})