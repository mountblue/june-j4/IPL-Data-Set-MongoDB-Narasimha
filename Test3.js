let expect = require("chai").expect;
let path = require("path");
let file = path.resolve("solutions.js");
let data = require(file);
const dataBaseName = "IPLtest";
const collectionMatches = "Question3m";
const collectionDeliveries = "Question3d";
const seasonToCheck = 2016;


describe("Extra runs conceded by the teams in the 2016 season of IPL", function () {
    it("Test Case 1: Positive Test Case", async function () {
        

        const expectedResult = [
            {_id: 'Mumbai Indians', extraRunsConceded: 4},
            {_id: 'Gujarat Lions', extraRunsConceded: 15},
            {_id: 'Rising Pune Supergiant', extraRunsConceded: 8},
            {_id: 'Kolkata Knight Riders', extraRunsConceded: 4}];


        const actualResult = await data.getExtraRunsConceded(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).deep.equals(expectedResult);
    })

    it("Test Case 2: Wrong values 1", async function () {
        const expectedResult = [
            {_id: 'Mumbai Indians', extraRunsConceded: 15},
            {_id: 'Gujarat Lions', extraRunsConceded: 4},
            {_id: 'Rising Pune Supergiant', extraRunsConceded: 4},
            {_id: 'Kolkata Knight Riders', extraRunsConceded: 8}];

        const actualResult = await data.getExtraRunsConceded(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 3: Empty values", async function () {
        const expectedResult = [];

        const actualResult = await data.getExtraRunsConceded(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).not.equals(expectedResult);
    })

    it("Test Case 4: Wrong DB", async function () {
        const expectedResult = [
            {_id: 'Mumbai Indians', extraRunsConceded: 4},
            {_id: 'Gujarat Lions', extraRunsConceded: 15},
            {_id: 'Rising Pune Supergiant', extraRunsConceded: 8},
            {_id: 'Kolkata Knight Riders', extraRunsConceded: 4}];

        const actualResult = await data.getExtraRunsConceded(dataBaseName, collectionMatches, collectionDeliveries, seasonToCheck);
        expect(actualResult).not.equals(expectedResult);
    })
})